// Mutator methods

let fruits = ['Apple', 'Orange', 'Kiwi', 'Dragon Fruit'];

// push() - adds an element in the end of an array AND returns the array's length

console.log('Current array:');
console.log(fruits);
let fruitsLength = fruits.push('Mango');
console.log(fruitsLength);
console.log('Mutated array from push method:');
console.log(fruits);

// Adding multiple elements in an array
fruits.push('Avocado', 'Guava');
console.log('Mutated array from push method:');
console.log(fruits);

// pop() - removes the last element in an array and returns the removed element
let removedFruit = fruits.pop();
console.log(removedFruit);
console.log('Mutated array from pop method: ');
console.log(fruits);

// unshift() - adds one or more elements at the beginning of an array
fruits.unshift('Lime', 'Banana');
console.log('Mutated array from unshift method');
console.log(fruits);

// shift() - removes an element at the beginning of an array AND returns the removed element
let anotherFruit = fruits.shift();
console.log(anotherFruit);
console.log('Mutated array from shift method:');
console.log(fruits);

// splice() - simultaneously removes elements from a specified index number and adds elements
/*
	Syntax:
		arrayName.splice(startingIndex, deleteCount, elementsToBeAdded)
*/

fruits.splice(1, 2, 'Lime');
console.log('Mutated array from splice method:');
console.log(fruits);

// sort() - rearranges the array elements in alphanumeric order
fruits.sort();
console.log("Mutated array from sort method");
console.log(fruits);

// reverse() - reverses the order of array elements
fruits.reverse();
console.log("Mutated array from reverse method:")
console.log(fruits);

// Non-mutator methods
let countries = ['US','PH', 'IN', 'SG', 'TH', 'PH', 'FR', 'DE'];

// indexOf() - return the index number of the first element found in an array
let firstIndex = countries.indexOf('PH');
console.log('Result of indexOf method: ' + firstIndex);

let invalidCountry = countries.indexOf('BR');
console.log('Result of indexOf method: ' + invalidCountry);

// lastIndexOf() - returns the index number of the last matching element found in an array.

// Getting the index number starting from the last element
let lastIndex = countries.lastIndexOf('PH');
console.log('Result of lastIndexOf method: ' + lastIndex);

// Getting the index number starting from a specified index
let lastIndexStart = countries.lastIndexOf('PH', 4);
console.log('Result of lastIndexOf method: ' + lastIndexStart);

// slice() - portions/slices elements from an array AND returns a new array

// Slicing off elements from a specified index to the last element
let slicedArrayA = countries.slice(2);
console.log('Result from slice method: ')
console.log(slicedArrayA);

// Slicing off elements from a specified index to another index.
let slicedArrayB = countries.slice(2, 4);
console.log('Result from the slice method: ');
console.log(slicedArrayB);

// Slicing off elements starting from the last element of an array
let slicedArrayC = countries.slice(-3);
console.log('Result from slice method:');
console.log(slicedArrayC)

// toString() - returns an array as a string separated by commas
let stringArray = countries.toString();
console.log('Result from toString method');
console.log(stringArray);

// concat() - combines two arrays and returns the combined result

let tasksArrayA = ['drink html', 'eat javascript'];
let tasksArrayB = ['inhale css', 'breathe sass'];
let tasksArrayC = ['get git', 'be node'];

let tasks = tasksArrayA.concat(tasksArrayB);
console.log('Result from concat method:');
console.log(tasks);

// combining multiple arrays
console.log('Result from concat method:');
let allTasks = tasksArrayA.concat(tasksArrayB, tasksArrayC);
console.log(allTasks);

// join() - returns an array as a string separated by specified separator string

let users = ['John', 'Jane', 'Joe', 'Robert'];

console.log(users.join());
console.log(users.join(''));
console.log(users.join(' - '));

// Iteration methods

// forEach() - similar to a for loop that iterates on each array element
/*
	Syntax:
		arrayName.forEach(function(indivElement) {statement})
*/

allTasks.forEach(function(task){
	console.log(task);
})

// Using forEach with conditional statements
console.log('Using forEach with conditional statements')
let filteredTasks = [];
allTasks.forEach(function(task){
	console.log(task);

	// If the element/string's length is greater that 10 characters
	if(task.length > 10){
		console.log(task);
		filteredTasks.push(task);
	}
})

console.log("Result of filtered tasks:");
console.log(filteredTasks)

// map() - iterate on each element AND returns a new array with different values depending on the result of the function's operation
console.log("Using map method")
let numbers = [1, 2, 3, 4, 5];

let numberMap = numbers.map(function(number){
	return number * number;
})
console.log("Original Array:")
console.log(numbers);
console.log("Result of map method");
console.log(numberMap);

console.log('Using forEach with operation:')
let numberForEach = numbers.forEach(function(number){
	return number * number;
})

console.log(numberForEach);

// every() - checks if all elements in an array meet the given condition

let allValid = numbers.every(function(number) {
	return (number < 3);
})

console.log("Result of every method:");
console.log(allValid);

// some() - checks if al least one element in the array meets the given condition

let someValid = numbers.some(function(number){
	return (number < 2);
})
console.log("Result of some method:")
console.log(someValid);

// filter() - returns new array that contains elements which meets the given condition

let filterValid = numbers.filter(function(number){
	return (number < 3);
})
console.log("Result of filter method:")
console.log(filterValid);

// includes() - checks if the argument contains the the list of the array
let products = ['Mouse', 'Keyboard', 'Laptop', 'Monitor'];

let productFound1 = products.includes("Mouse");

console.log("Result of includes method:")
console.log(productFound1);

let productFound2 = products.includes("Headset");
console.log(productFound2);

// reduce() - evaluates elements from left to right and returns/reduces the array into a single value

let iteration = 0;

let reducedArray = numbers.reduce(function(x, y){
	console.warn('current iteration: ' + ++iteration);
	console.log('accumulator: ' + x);
	console.log('currentValue: ' + y)

	// The operation to reduce the array into a single value
	return x + y;
})
console.log('Result of reduce method: ' + reducedArray);
// 1 2 3 4 5
